package com.bg.ppoketmonapi.service;

import com.bg.ppoketmonapi.entity.Ppoketmon;
import com.bg.ppoketmonapi.model.PpoketmonItem;
import com.bg.ppoketmonapi.repository.PpoketmonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PpoketmonService {
    private final PpoketmonRepository ppoketmonRepository;

    public Ppoketmon getData(long id) {return ppoketmonRepository.findById(id).orElseThrow();}

    public List<PpoketmonItem> getPpoketmons() {
        List<Ppoketmon> originlist = ppoketmonRepository.findAll();

        List<PpoketmonItem> result = new LinkedList<>();

        for (Ppoketmon ppoketmon : originlist) {
            PpoketmonItem addItem = new PpoketmonItem();
            addItem.setId(ppoketmon.getId());
            addItem.setPocketImg(ppoketmon.getPocketImg());
            addItem.setPocketName(ppoketmon.getPocketName());
            addItem.setPocketType(ppoketmon.getPocketType());

            result.add(addItem);
        }
        return result;
    }
}
