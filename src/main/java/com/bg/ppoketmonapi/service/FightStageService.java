package com.bg.ppoketmonapi.service;

import com.bg.ppoketmonapi.entity.FightStage;
import com.bg.ppoketmonapi.entity.Ppoketmon;
import com.bg.ppoketmonapi.model.FightStageInPpoketmonItem;
import com.bg.ppoketmonapi.model.FightStageInPpoketmonsResponse;
import com.bg.ppoketmonapi.repository.FightStageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FightStageService {
    private final FightStageRepository fightStageRepository;

    public void setStage(Ppoketmon ppoketmon) throws Exception {
        List<FightStage> checkList = fightStageRepository.findAll();

        if (checkList.size() >= 2) throw new Exception();

        FightStage fightStage = new FightStage();
        fightStage.setPpoketmon(ppoketmon);
        fightStageRepository.save(fightStage);
    }

    public FightStageInPpoketmonsResponse getCurrentState() {
        List<FightStage> checkList = fightStageRepository.findAll();

        FightStageInPpoketmonsResponse response = new FightStageInPpoketmonsResponse();

        if (checkList.size() == 2) {
            response.setPpoketmon1(converPpoketmonItem(checkList.get(0)));
            response.setPpoketmon2(converPpoketmonItem(checkList.get(1)));
        } else if (checkList.size() == 2) {
            response.setPpoketmon1(converPpoketmonItem(checkList.get(0)));
        }
        return response;
    }

    private FightStageInPpoketmonItem converPpoketmonItem(FightStage fightStage) {
        FightStageInPpoketmonItem ppoketmonItem = new FightStageInPpoketmonItem();
        ppoketmonItem.setPpoketmonStageId(fightStage.getId());
        ppoketmonItem.setPpoketmonId(fightStage.getPpoketmon().getId());
        ppoketmonItem.setPpoketmonName(fightStage.getPpoketmon().getPocketName());
        ppoketmonItem.setPpoketmonHp(fightStage.getPpoketmon().getHp());
        ppoketmonItem.setPpoketmonOffensive(fightStage.getPpoketmon().getOffensive());
        ppoketmonItem.setPpoketmonDefensive(fightStage.getPpoketmon().getDefensive());
        ppoketmonItem.setPpoketmonSpeed(fightStage.getPpoketmon().getSpeed());

        return ppoketmonItem;
    }
}
