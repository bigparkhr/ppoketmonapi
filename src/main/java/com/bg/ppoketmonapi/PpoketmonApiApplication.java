package com.bg.ppoketmonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpoketmonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpoketmonApiApplication.class, args);
	}

}
