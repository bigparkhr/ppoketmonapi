package com.bg.ppoketmonapi.repository;

import com.bg.ppoketmonapi.entity.Ppoketmon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PpoketmonRepository extends JpaRepository<Ppoketmon, Long> {
}
