package com.bg.ppoketmonapi.repository;

import com.bg.ppoketmonapi.entity.FightStage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FightStageRepository extends JpaRepository<FightStage, Long> {
}
