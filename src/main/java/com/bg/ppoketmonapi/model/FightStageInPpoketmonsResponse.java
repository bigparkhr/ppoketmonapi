package com.bg.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FightStageInPpoketmonsResponse {
    private FightStageInPpoketmonItem Ppoketmon1;
    private FightStageInPpoketmonItem Ppoketmon2;
}
