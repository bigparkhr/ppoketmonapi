package com.bg.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PpoketmonItem {
    private Long id;
    private String pocketImg;
    private String pocketName;
    private String pocketType;
}
