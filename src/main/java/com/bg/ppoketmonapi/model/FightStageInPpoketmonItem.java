package com.bg.ppoketmonapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FightStageInPpoketmonItem {
    private Long ppoketmonStageId;
    private Long ppoketmonId;
    private String ppoketmonName;
    private Integer ppoketmonHp;
    private Integer ppoketmonSpeed;
    private Integer ppoketmonOffensive;
    private Integer ppoketmonDefensive;
}
