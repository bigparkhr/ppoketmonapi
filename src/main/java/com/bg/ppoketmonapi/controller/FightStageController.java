package com.bg.ppoketmonapi.controller;

import com.bg.ppoketmonapi.entity.Ppoketmon;
import com.bg.ppoketmonapi.model.CommonResult;
import com.bg.ppoketmonapi.service.FightStageService;
import com.bg.ppoketmonapi.service.PpoketmonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fight-stage")
public class FightStageController {
    private final PpoketmonService ppoketmonService;
    private final FightStageService fightStageService;

    @PostMapping("/new/fight-id/{id}")
    public CommonResult setStage(@PathVariable long id) throws Exception {
        Ppoketmon ppoketmon = ppoketmonService.getData(id);
        fightStageService.setStage(ppoketmon);

        CommonResult commonResult = new CommonResult();
        commonResult.setCode(0);
        commonResult.setMsg("성공하였습니다.");

        return commonResult;
    }
}
