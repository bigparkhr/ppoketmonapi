package com.bg.ppoketmonapi.controller;

import com.bg.ppoketmonapi.model.CommonResult;
import com.bg.ppoketmonapi.model.ListResult;
import com.bg.ppoketmonapi.model.PpoketmonItem;
import com.bg.ppoketmonapi.service.PpoketmonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ppoketmon")
public class PpoketmonController {
    private final PpoketmonService ppoketmonService;

    @GetMapping("/all")
    public ListResult<PpoketmonItem> getPpoketmons() {
        List<PpoketmonItem> List = ppoketmonService.getPpoketmons();

        ListResult<PpoketmonItem> response = new ListResult<>();
        response.setList(List);
        response.setTotalCount(List.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }
}
