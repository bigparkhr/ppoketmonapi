package com.bg.ppoketmonapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Ppoketmon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, length = 20)
    private String pocketImg;

    @Column(nullable = false, length = 15)
    private String pocketName;

    @Column(nullable = false, length = 10)
    private String pocketType;

    @Column(nullable = false)
    private Integer hp;

    @Column(nullable = false)
    private Integer speed;

    @Column(nullable = false)
    private Integer offensive;

    @Column(nullable = false)
    private Integer defensive;

    @Column(nullable = false)
    private Integer evolutionGrade;

    private String etcMemo;
}
